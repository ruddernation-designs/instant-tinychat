<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Ruddernation Designs&trade;</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Ruddernation Designs"/>
<meta name="Copyright" content="Ruddernation Designs 1998-2016"/>
<link rel="shortcut icon" href="https://www.ruddernation.com/info/images/ico/favicon.ico">
<link rel="stylesheet" href="https://www.ruddernation.com/info/backgrounds/background.css"/>
<link rel="stylesheet" href="css/chat.css">
</head>
<body>
<?php 
$room = filter_input(INPUT_POST, 'room');
if ($room = filter_input(INPUT_SERVER, 'REQUEST_URI'));
if(preg_match("/^[a-z0-9]/",$room=strtolower($room)));
$room=preg_replace('/[^a-zA-Z0-9]/','',$room);
if (strlen($room) < 3)
$room = substr($room, 0, 0);
if (strlen($room) > 36)
$room = substr($room, 0, 0);
$room=strip_tags($room);
$prohash = hash('sha256',filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
if(empty($room))
{
	echo '<p>Type a room name into the URL bar! <br/>example: <a href="http://www.ruddernation.info/ruddernation">http://www.ruddernation.info/ruddernation</a></p>';
return;
}
$chat = array(
		'room' 			=> $room,
		'prohash'		=> $prohash,
		'chatSmileys'   	=> 'true',
		'wmode'			=> 'transparent',
		'youtube'		=> 'all',
		'urlsuper'		=> 'https://www.ruddernation.com',
		'desktop'		=> 'true', 
		'langdefault'		=>  'en' ); 
$chat['room'] = $room;
	foreach ( $chat as $r => $u ) { $chatString .= "{$r}: '{$u}', "; 
}
	$chat = substr( $chatString, 0); ?>
<div id="chat">
<script type=text/javascript>var tinychat = ({<?php echo $chat; ?>})</script>
<script src="https://www.ruddernation.com/info/js/eslag.js"></script>
<div id="client"> </div></div>
</body>
</html>
