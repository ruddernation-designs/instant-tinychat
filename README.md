# Instant-Tinychat
<br/>
This is a bare minimum of PHP code need to start your own instant chat site, You'd just input the room name after your domain name,
To use this you must make sure your hosting provider allows you to edit the .htaccess file, 
I will provide the code inside the file, I've also porvided minimal css and html, You can change all to suit your needs,
You will also need to include your own PHP code for security but with the preg_replace that covers most.

This is using my chat script file to load if you wish to use Tinychat's then just change the link to 
`http://tinychat.com/js/embed.js`
You can also add or remove features from the array itself, 
Also you could use `str_replace instead of preg_replace` as this to is also faster but the code is slimmed down so much that you wouldn't really notice it.

To use this code then download and place the index.php, css folder and the .htaccess to your server,
You may need to change the folder you have access to the chat but I've done this for the local root so the chat would work using
`<a href="/">http://www.yourdomain.com/roomname</a>` It will also ignore the index page it wont display as 
`<a href="/">http://www.yourdomain.com/index.php?roomname</a>`.
